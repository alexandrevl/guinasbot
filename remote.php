<!DOCTYPE html>
<html>
<?php
function getUserIpAddr(){
    if(!empty($_SERVER['HTTP_CLIENT_IP'])){
        //ip from share internet
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
        //ip pass from proxy
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }else{
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

// $ip = getUserIpAddr();
// $token = md5(date('YWH') . getUserIpAddr());
// if($_GET["token"] === "") {
//     header("Location: https://bot.mrguinas.com.br/login.php");
//     die();
// } else {
//     $tokenGet = $_GET["token"];
//     if ($token != $tokenGet) {
//         header("Location: https://bot.mrguinas.com.br/login.php");
//         die();
//     }
// }
 ?>

<head>
    <meta charset='utf-8'>
    <meta http-equiv='content-type' content='text/html;charset=UTF-8'>
    <title>GuinasBot</title>
    <link rel="icon" type="image/png" href="Logo.png" />
    <style>
    .iframe-container {
        position: relative;
        overflow: hidden;
        padding-top: 56.25%;
    }

    .iframe-container iframe {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        border: 0;
    }
    </style>
    <script>
    if (typeof module === 'object') {
        window.module = module;
        module = undefined;
    }
    </script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <script src="https://kit.fontawesome.com/eb450fa17a.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/locale/pt-br.js'></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.11/lodash.min.js'></script>
    <script type="text/javascript" src="node_modules/obs-websocket-js/dist/obs-websocket.js"></script>
    <script src="https://apis.google.com/js/client.js?onload=googleApiClientReady"></script>
    <script src='js/remote.js?v=16'></script>
    <!-- Insert this line after script imports -->
    <script>
    if (window.module) module = window.module;
    </script>
</head>


<body>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3>Remote BOT Infos</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <nav class="navbar navbar-light bg-light">
                    <div class="d-flex flex-row bd-highlight mb-3">
                        <div class="p-2 bd-highlight" id="channelId"></div>
                        <div class="p-2 bd-highlight" id="obs"></div>
                        <div class="p-2 bd-highlight" id="qntMessages"></div>
                        <div class="p-2 bd-highlight" id="poolingTime"></div>
                        <div class="p-2 bd-highlight" id="totalResults"></div>
                        <div class="p-2 bd-highlight" id="wordsCount"></div>
                    </div>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div id="chat" class="bg-light text-monospace"></div>
            </div>
        </div>

    </div>
</body>

</html>