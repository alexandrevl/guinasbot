var channelId = "UCCFcBOBr7B3ob4JjyTRdTRQ";
if (getParameter("c")) {
	channelId = getParameter("c");
}

$(document).ready(function() {
	check();
	setInterval(function() {
		check();
	}, 500);
});

function check() {
	var jsonPost = {
		id_bot: channelId
	};
	$.ajax({
		type: "POST",
		url: "getInfestacao.php",
		contentType: "application/json",
		dataType: "json",
		data: JSON.stringify(jsonPost),
		cache: false,
		statusCode: {
			200: function(response) {
				//console.log(response);
				if (response.isActive) {
					$("#displayProgress").show();
					var now = Date.now();
					var dif = response.dueTime - now;
					var percent = 100;
					if (dif >= 0) {
						percent = 100 - Math.round((dif / 135000) * 100);
						//console.log(dif);
					}
					$("#opt0").attr("aria-valuenow", "" + percent + "");
					$("#opt0").attr("style", "width: " + percent + "%");
					//$("#opt0").text("" + percent + "%");
				} else {
					$("#displayProgress").hide();
				}
			}
		},
		404: function(resultData) {}
	});
}

function getParameter(parameterName) {
	var result = null,
		tmp = [];
	location.search
		.substr(1)
		.split("&")
		.forEach(function(item) {
			tmp = item.split("=");
			if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
		});
	return result;
}
