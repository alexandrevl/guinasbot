$(document).ready(function() {
	for (let index = 1; index <= 30; index++) {
		appendLog("<br>");
	}
	appendLog("Welcome remote");
	check();
	setInterval(function() {
		check();
	}, 1000);
});

function check() {
	var jsonPost = {};
	//console.log(jsonPost);
	$.ajax({
		type: "POST",
		url: "getRemote.php",
		contentType: "application/json",
		dataType: "json",
		data: JSON.stringify(jsonPost),
		cache: false,
		statusCode: {
			200: function(response) {
				//console.log(response);
				response.forEach(item => {
					var msg = JSON.parse(item.msg);
					//console.log(item.msg);
					if (msg.type == 1) {
						appendLog(msg.msg);
					} else if (msg.type == 2) {
						$("#channelId").text("Chan: " + msg.msg.channelTitle);
						$("#obs").text("Obs: " + (msg.msg.obs ? "On" : "Off"));
						$("#qntMessages").text("Msg: " + msg.msg.qntMessages);
						$("#poolingTime").text("Pool: " + msg.msg.pool);
						$("#totalResults").text("Total: " + msg.msg.total);
					}
				});
			},
			404: function(resultData) {
				//console.log(resultData);
			}
		}
	});
}
function appendLog(msg) {
	if ($("#chat").children().length >= 30) {
		$("#chat")
			.children()[0]
			.remove();
	}
	$("#chat").append('<div class="bg-light"> [' + moment().format("HH:mm:ss") + "] > " + msg + "</div>");
}
