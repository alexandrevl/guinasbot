$(document).ready(function() {
    $("#signIn").click(function() {
        login();
    });
    $("#inputPassword").keypress(function(e) {
        var key = e.which;
        if (key == 13) {
            login();
        }
    });  
});

function login() {
    $("#alert").hide();

    var jsonPost = {
        "password": $("#inputPassword").val()
    };
    $.ajax({
        type: 'POST',
        url: "https://bot.mrguinas.com.br/checkPassword.php",
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(jsonPost),
        cache: false,
        statusCode: {
            200: function(resultData) {
                if (resultData.responseText != "not ok") { 
                    console.log(resultData.responseText);
                    window.location = "https://bot.mrguinas.com.br/?token=" + resultData.responseText;
                } else {
                    $("#alert").text(resultData.responseText);
                    $("#alert").show();
                    //console.log(resultData);
                }
            },
            404: function(resultData) {
                console.log("erro");
            }
        }
    }).fail(function() {
        // console.log("error");
    });
}