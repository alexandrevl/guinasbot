$(document).ready(function() {
	$("#btnResetRanking").click(function() {
		ranking = [];
		var jsonPost = {
			dados: []
		};
		$.ajax({
			type: "POST",
			url: "https://bot.mrguinas.com.br/setRanking.php",
			contentType: "application/json",
			dataType: "json",
			data: JSON.stringify(jsonPost),
			cache: false,
			statusCode: {
				200: function(response) {
					loadRanking();
				},
				404: function(resultData) {}
			}
		});
	});
	$("#btnStats").click(function() {
		loadRanking();
	});
});
function loadRanking() {
	$("#tbody").empty();
	$("#listTable > thead").replaceWith(
		"<tr>" +
			'<th class="text-center">#</th>' +
			'<th class="text-center">User</th>' +
			"<th class='text-truncate'>Nome</th>" +
			'<th class="text-center">Acertos</th>' +
			"</tr>"
	);
	var jsonPost = {};
	$.ajax({
		type: "POST",
		url: "https://bot.mrguinas.com.br/getRanking.php",
		contentType: "application/json",
		dataType: "json",
		data: JSON.stringify(jsonPost),
		cache: false,
		statusCode: {
			200: function(resultData) {
				var json = resultData;
				$("#titleModal").text("Ranking");
				var i = 1;
				if (resultData.length > 0) {
					resultDataOrdered = _.orderBy(resultData, ["hits"], ["desc"]);
					resultDataOrdered.forEach(function(users) {
						$("#listTable > tbody:last-child").append(
							"<tr>" +
								'<th scope="row" class="text-center">' +
								i++ +
								"</th>" +
								'<td class="text-center"><a href="' +
								users.author.channelUrl +
								'" target="#"><img id="img' +
								users.author.channelId +
								'" width="30%" src="' +
								users.author.profileImageUrl +
								'" class="rounded-circle" alt=""></a></td>' +
								"<td class='text-truncate'>" +
								users.author.displayName +
								"</td>" +
								'<td class="text-center">' +
								users.hits +
								"</td>" +
								"</tr>"
						);
					}, this);
				} else {
					$("#listTable > tbody:last-child").append(
						"<tr>" +
							'<th scope="row" class="text-center" colspan="4"> Clean ranking</th>' +
							"</tr>"
					);
				}
			},
			404: function(resultData) {}
		}
	}).fail(function() {
		//console.log("error");
	});
}
