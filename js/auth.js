// The client ID is obtained from the Google Developers Console
// at https://console.developers.google.com/.
// If you run this code from a server other than http://localhost,
// you need to register your own client ID.
var OAUTH2_CLIENT_ID = "719120864815-hduptsmu97uoobf9c2243useo67t16i1.apps.googleusercontent.com";
var OAUTH2_SCOPES = ["https://www.googleapis.com/auth/youtube", "https://www.googleapis.com/auth/youtube.force-ssl"];

// Upon loading, the Google APIs JS client automatically invokes this callback.
googleApiClientReady = function() {
	gapi.auth.init(function() {
		window.setTimeout(checkAuth, 1);
	});
};

// Attempt the immediate OAuth 2.0 client flow as soon as the page loads.
// If the currently logged-in Google Account has previously authorized
// the client specified as the OAUTH2_CLIENT_ID, then the authorization
// succeeds with no user intervention. Otherwise, it fails and the
// user interface that prompts for authorization needs to display.
function checkAuth() {
	gapi.auth.authorize(
		{
			client_id: OAUTH2_CLIENT_ID,
			scope: OAUTH2_SCOPES,
			immediate: true
		},
		handleAuthResult
	);

	appendLog("YT Trying auth youtube");
}

// Handle the result of a gapi.auth.authorize() call.
function handleAuthResult(authResult) {
	appendLog("YT Handle the result of a gapi.auth.authorize() call.");
	if (authResult && !authResult.error) {
		$("#login").hide();
		appendLog("YT Auth youtube ok");
		// Authorization was successful. Hide authorization prompts and show
		// content that should be visible after authorization succeeds.
		loadAPIClientInterfaces();
	} else {
		appendLog("Error", authResult);
		// Make the #login-link clickable. Attempt a non-immediate OAuth 2.0
		// client flow. The current function is called when that flow completes.
		$("#bot").text("");
		$("#login").show();
		$("#status").hide();
	}
}

// Load the client interfaces for the YouTube Analytics and Data APIs, which
// are required to use the Google APIs JS client. More info is available at
// http://code.google.com/p/google-api-javascript-client/wiki/GettingStarted#Loading_the_Client
function loadAPIClientInterfaces() {
	appendLog("YT Loading the client interfaces for the YouTube");
	gapi.client.load("youtube", "v3", function() {
		handleAPILoaded();
	});
}

function appendLog(msg) {
	if ($("#chat").children().length >= 10) {
		$("#chat")
			.children()[0]
			.remove();
	}
	$("#chat").append('<div class="bg-light"> [' + moment().format("HH:mm:ss") + "] > " + msg + "</div>");
	if (msg != "<br>") {
		sendRemote({ type: 1, msg: msg });
	}
}

function sendRemote(msg) {
	var jsonPost = {
		id: Date.now(),
		msg: JSON.stringify(msg)
	};
	//console.log(jsonPost);
	$.ajax({
		type: "POST",
		url: "addRemote.php",
		contentType: "application/json",
		dataType: "json",
		data: JSON.stringify(jsonPost),
		cache: false,
		statusCode: {
			200: function(response) {
				//console.log(response);
			},
			404: function(resultData) {
				//console.log(resultData);
			}
		}
	});
}
