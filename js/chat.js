const key = "AIzaSyDMIgGgUIQtpl-MwYEPI0usf4OiK0kQNYU";
//MrGuinas
var channelId = "UCCFcBOBr7B3ob4JjyTRdTRQ";
if (getParameter("c")) {
    channelId = getParameter("c");
}
//const channelId = 'UCXgs_fVuGdFYhDaCVrNp3KQ';
var video = {
    liveChatId: null,
    videoId: null
};
var ranking = [];
var infestacao = {};
var channelTitle = null;

function resetInfestacao() {
    queuePunishmentList = [];
    clearTimeout(infestacao.timeoutVar);
    clearTimeout(infestacao.timeoutQuestion);
    infestacao.timeoutQuestion = null;
    infestacao = {
        isOn: infestacao.isOn,
        isActive: false,
        hit: {
            isSuperchat: false,
            isHit: false,
            author: null
        },
        word: null,
        minSec: 10,
        maxSec: 30,
        punishment: {},
        timeoutVar: null,
        timeoutQuestion: null,
        authorName: null,
        dueTime: 0,
        question: null
    };
    duePunishment = 0;
    isSettingPunishment = false;
    appendLog("Infestação: RESET - All processes stopped!");
    clearInfestacao(false);
}

var kbitsPerSec = 2500;
var isBotModerator = true;
var isFirst = true;
var lastEtag = null;
var qntMessages = 0;
var poll = [];
var isObsConnected = false;
var isWords = false;
var jsonWords = [];
const obs = new OBSWebSocket();

$(document).ready(function() {
    $("#login").click(function() {
        gapi.auth.authorize({
                client_id: OAUTH2_CLIENT_ID,
                scope: OAUTH2_SCOPES,
                immediate: false,
                cookie_policy: "single_host_origin"
            },
            handleAuthResult
        );
    });
    getWords();
    for (let index = 1; index <= 20; index++) {
        appendLog("<br>");
    }
    // setPoll();
    $('#resetPolls').click(function() {
        poll = [];
        //$("#polls").text(poll.toString());
        setPoll();
    });
    $('#resetWords').click(function() {
        jsonWords = [];
        //$("#polls").text(poll.toString());
        $("#wordsCount").text("Words: " + jsonWords.length);
        setWords();
        appendLog("Palavras: RESET");
    });
    $('#btnWords').click(function() {
        isWords = !isWords;
        if (isWords) {
            appendLog("Palavras: ON");
            $("#btnWords").attr("class", "btn-sm btn-danger");
            $("#btnWords").text("Desligar Palavras");
            $("#resetWords").show();
            $("#displayWords").show();
        } else {
            appendLog("Palavras: OFF");
            $("#btnWords").text("Ligar Palavras");
            $("#btnWords").attr("class", "btn-sm btn-success");
            $("#resetWords").hide();
             $("#displayWords").hide();
        }
    });
    $("#btnInfestacao").click(function() {
        infestacao.isOn = !infestacao.isOn;
        if (infestacao.isOn) {
            appendLog("Infestação: ON");
            $("#btnInfestacao").attr("class", "btn-sm btn-danger");
        } else {
            queuePunishmentList = [];
            isQuestionRunning = false;
            appendLog("Infestação: OFF");
            infestacao.dueTime = 0;
            duePunishment = 0;
            isSettingPunishment = false;
            //resetInfestacao();
            infestacao.hit.isHit = false;
            infestacao.hit.isSuperchat = false;
            $("#btnInfestacao").attr("class", "btn-sm btn-success");
        }
        setInfestacao();
        clearInfestacao(false);
    });
    $("#btnReset").click(function() {
        resetInfestacao();
    });
    $("#status").text("Connecting...");
    //initClient();
    //checkAuth();
});
var heartEmoji = ["💜", "💙", "💚", "💛", "❤", "💓", "💗", "💟", "💝", "💖"];

var actions = [{
        word: ["1", "2"],
        action: "poll",
        param: ""
    }
    // {
    //     word: "ban",
    //     action: "scene",
    //     param: "Scene 3"
    // },
    // {
    //     word: "xixi",
    //     action: "scene",
    //     param: "Scene 2"
    // },
    // {
    //     word: "guinas",
    //     action: "scene",
    //     param: "Scene 4"
    // },
    // {
    //     word: ["butico", "guinas"],
    //     action: "text",
    //     param: "ban"
    // }
];

async function main() {
    $("#status").text("Connecting...");
    var isLive = await getLiveChatId();
    gapi.client.youtube.channels
        .list({
            part: "snippet",
            mine: true
        })
        .then(
            function(response) {
                // Handle the results here (response.result has the parsed body).
                //appendLog("Response", response.result);
                //$('#bot').html('<img src="' + response.result.items[0].snippet.thumbnails.default.url + '"width="10%"> Bot: ' + response.result.items[0].snippet.title.trim());
                $("#login").html("Bot: " + response.result.items[0].snippet.title.trim());
                $("#login").show();
                $("#status").hide();
            },
            function(err) {
                console.error("Execute error", err);
            }
        );
    gapi.client.youtube.channels
        .list({
            part: "snippet",
            id: channelId
        })
        .then(
            function(response) {
                // Handle the results here (response.result has the parsed body).
                // appendLog("Response", response.result);
                channelTitle = response.result.items[0].snippet.title;
                $("#channelId").text(
                    "Chan: " + channelTitle
                );
            },
            function(err) {
                console.error("Execute error", err);
            }
        );
    if (isLive) {
        appendLog("YT Live: On");
        $("#liveStatus").hide();
        $("#displayBtn").show();
        const address = "127.0.0.1:4444";
        appendLog("OBS Connecting...");
        obs.connect({ address: address }, (err, data) => {
            if (err) {
                appendLog("OBS Not connected");
                $("#obs").text("Obs: Off");
            } else {
                appendLog("OBS Connected");
                setQuestion();
                $("#obs").text("Obs: On");
                // obs.on("Heartbeat", data => {
                //     if (data.streaming == true) {
                //         //appendLog("OBS Streaming: Live");
                //     } else {
                //         //appendLog("OBS Streaming: offline");
                //     }
                // });
                // obs.on("StreamStatus", data => {
                //     if (data.streaming == true) {
                //         if (data.kbitsPerSec / kbitsPerSec < 0.8) {
                //             appendLog("OBS StreamStatus: problema vemelho");
                //         } else if (data.kbitsPerSec / kbitsPerSec < 0.9) {
                //             appendLog("OBS StreamStatus: problema laranja");
                //         } else {
                //             appendLog("OBS StreamStatus: verde");
                //         }
                //     }
                // });
                isObsConnected = true;
            }
        });
        getMessages(null);
    } else {
        //$('#bot').text("");
        $("#liveStatus").text("Live: Off");
        appendLog("YT Live: Off");
        setTimeout(function() {
            main();
        }, 10000);
    }
}

function getLiveChatId() {
    return new Promise(resolve => {
        appendLog("YT Detecting livestream");
        if (video.liveChatId == null) {
            var getVideoIdOptions =
                "https://www.googleapis.com/youtube/v3/search?part=snippet&eventType=live&type=video&maxResults=10&channelId=" +
                channelId +
                "&key=" +
                key;
            $.ajax({
                type: "GET",
                url: getVideoIdOptions,
                contentType: "application/json",
                cache: false,
                statusCode: {
                    200: function(response) {
                        //appendLog(response);
                        if (response.items.length > 0) {
                            appendLog("YT Getting livestream properties");
                            video.videoId = response.items[0].id.videoId;
                            var getViewerOptions =
                                "https://www.googleapis.com/youtube/v3/videos?part=liveStreamingDetails,statistics&id=" +
                                video.videoId +
                                "&key=" +
                                key;
                            $.ajax({
                                type: "GET",
                                url: getViewerOptions,
                                contentType: "application/json",
                                cache: false,
                                statusCode: {
                                    200: function(response) {
                                        // appendLog(response);
                                        //$("#polls").text(poll.toString());
                                        video.liveChatId =
                                            response.items[0].liveStreamingDetails.activeLiveChatId;
                                        $("#liveChatId").text(video.liveChatId);
                                        //$('#resetPolls').show();
                                        // gapi.client.youtube.liveChatModerators.list({
                                        //         "liveChatId": video.liveChatId,
                                        //         "part": "id"
                                        //     })
                                        //     .then(function(response) {
                                        //             // Handle the results here (response.result has the parsed body).
                                        //             //appendLog("Response", response);
                                        //             isBotModerator = true;
                                        //             appendLog("Bot is Moderator");
                                        //         },
                                        //         function(err) {
                                        //             isBotModerator = true;
                                        //             appendLog("Bot isn't Moderator");
                                        //             //console.error("Execute error", err);
                                        //         });

                                        // appendLog(video);
                                        resolve(true);
                                    },
                                    400: function(resultData) {
                                        video.liveChatId = null;
                                        video.videoId = null;
                                        appendLog("Erro: " + resultData);
                                        resolve(false);
                                    },
                                    403: function(resultData) {
                                        video.liveChatId = null;
                                        video.videoId = null;
                                        appendLog("Erro: " + resultData);
                                        resolve(false);
                                    },
                                    404: function(resultData) {
                                        video.liveChatId = null;
                                        video.videoId = null;
                                        appendLog("Erro: " + resultData);
                                        resolve(false);
                                    }
                                }
                            });
                        } else {
                            resolve(false);
                        }
                    },
                    400: function(resultData) {
                        video.liveChatId = null;
                        video.videoId = null;
                        appendLog("Erro: " + resultData);
                        resolve(false);
                    },
                    404: function(resultData) {
                        video.liveChatId = null;
                        video.videoId = null;
                        appendLog("Erro: " + resultData);
                        resolve(false);
                    }
                }
            });
        } else {
            resolve(true);
        }
    });
}
var msgControl = [];
var msgControlQnt = [];

function getMessages(nextPageToken) {
    //appendLog(msgControl);
    //wordsArray = [];
    getWords();;
    return new Promise(resolve => {
        var listMessages = null;
        if (!nextPageToken) {
            listMessages =
                "https://www.googleapis.com/youtube/v3/liveChat/messages?liveChatId=" +
                video.liveChatId +
                "&part=snippet,authorDetails&maxResults=2000&key=" +
                key;
        } else {
            listMessages =
                "https://www.googleapis.com/youtube/v3/liveChat/messages?liveChatId=" +
                video.liveChatId +
                "&part=snippet,authorDetails&pageToken=" +
                nextPageToken +
                "&key=" +
                key;
        }
        $.ajax({
            type: "GET",
            url: listMessages,
            contentType: "application/json",
            cache: false,
            statusCode: {
                200: function(response) {
                    //appendLog(response);
                    var show = false;
                    var isPoll = false;
                    //var wordsArray = [];
                    response.items.forEach(chatMessage => {
                        //appendLog(chatMessage);
                        //console.log(chatMessage);
                        var msg = chatMessage.snippet.displayMessage;
                        var author = chatMessage.authorDetails;
                        var publishedAt = chatMessage.snippet.publishedAt;
                        var childs = 0;
                        if (!isFirst && author.channelId != 'UCSvjQBDgYDB5TGVmCZObcwA') {
                            ++qntMessages;
                            if (isWords) {
                               //console.log(author);
                               var splitMsg = convertToSlug(msg).split("-"); 
                               splitMsg.forEach(word => {
                                  switch (word) {
                                      case "s":
                                          word = "sim";
                                          break;
                                      case "n":
                                          word = "nao";
                                          break;
                                      case "vc":
                                          word = "voce";
                                          break;
                                      case "tb":
                                          word = "tambem";
                                          break;
                                      case "ctz":
                                          word = "certeza";
                                          break;
                                      case "fnaf":
                                      case "sexo":
                                      case "komodo":
                                      case "xvideos":
                                          word = "x";
                                          break;
                                              
                                      default:
                                          break;
                                  }
                                   if (word.length > 0 && (word == "sim" || word == "nao" || word.length > 3 || !isNaN(word))) {
                                       switch (word) {
                                            // case "esse":
                                            // case "isso":
                                            // case "aquilo": 
                                            // case "aquele":
                                            case "isto":  
                                               break;
                                           default:
                                                var findIndex = _.findIndex(wordsArray, ['text', word]);
                                                if (findIndex > -1) {
                                                    wordsArray[findIndex].weight++;
                                                } else {
                                                    wordsArray.push({text: word, weight: 1});
                                                }
                                               break;
                                       }
                                   }
                                   
                               });
                            }

                            if (infestacao.isOn) {
                                if (chatMessage.snippet.superChatDetails) {
                                    //console.log(chatMessage.snippet.superChatDetails);
                                    //{amountMicros: "1000000", currency: "BRL", amountDisplayString: "R$1.00", tier: 1}
                                    
                                    
                                    //clearTimeout(infestacao.timeoutVar);
                                    //clearTimeout(infestacao.timeoutQuestion);
                                    //infestacao.timeoutQuestion = null;
                                    randomPunishment(Math.round(chatMessage.snippet.superChatDetails.amountMicros/10000));
                                    appendLog(
                                        "Superchat: " +
                                        author.displayName
                                    );
                                    var punishment = {
                                        isSuperchat: true,
                                        isHit: false,
                                        author: author
                                    };
                                    queuePunishment(punishment);
                                    sendMessage(
                                        "INFESTAÇÃO: Puniç˜ão Superchat. Obrigado, " + author.displayName);
                                }
                                if (infestacao.isActive) {
                                    if (convertToSlug(msg).includes(convertToSlug(infestacao.word))) {
                                        randomPunishment(0);
                                        appendLog(
                                            "Hit Word: " +
                                            author.displayName
                                        );
                                        var punishment = {
                                            isSuperchat: false,
                                            isHit: true,
                                            author: author
                                        };
                                        infestacao.punishment.word = infestacao.word;
                                        queuePunishment(punishment);
                                        sendMessage("INFESTAÇÃO ACERTO: " + infestacao.word.toLowerCase() + " - " + author.displayName);
                                    }
                                }
                            }
                        }


                        lastEtag = chatMessage.etag;
                        //     }
                        // }
                    });
                    //console.log(wordsArray);
                    if (isWords) {
                        wordsArray.forEach(word => {
                            var findIndex = _.findIndex(jsonWords, ['text', word.text]);
                            if (findIndex > -1) { 
                                jsonWords[findIndex].weight += word.weight;
                            } else {
                                jsonWords.push({text: word.text, weight: word.weight});
                            }
                        });
                        jsonWords = _.orderBy(jsonWords,['weight'], ['desc']);
                        //console.log(jsonWords);
                        setWords();
                        $("#wordsCount").text("Words: " + jsonWords.length + "/" + _.sumBy(jsonWords, (o) => { return o.weight; }));
                    }
                    if (!show) {
                        lastEtag = null;
                    }
                    isFirst = false;
                    $("#qntMessages").text("Msg: " + qntMessages);
                    $("#poolingTime").text(
                        "Pool: " + parseInt(response.pollingIntervalMillis)/1000
                    );
                    $("#totalResults").text(
                        "Total: " + parseInt(response.pageInfo.totalResults) + "/" + parseInt(response.pageInfo.resultsPerPage)
                    );
                    sendRemote({ 
                        type: 2, 
                        msg: { 
                            channelTitle: channelTitle,
                            obs: isObsConnected,
                            pool: (parseInt(response.pollingIntervalMillis)/1000).toFixed(2), 
                            qntMessages: qntMessages, 
                            total: (parseInt(response.pageInfo.totalResults) + "/" + parseInt(response.pageInfo.resultsPerPage))
                        }
                    });
                    
                    setTimeout(function() {
                        getMessages(response.nextPageToken);
                    }, parseInt(response.pollingIntervalMillis));
                    var timerDiv = Math.round(response.pollingIntervalMillis / 1000);
                    for (let index = timerDiv; index > -1; index--) {
                        var left = (timerDiv - index) * 1000;
                        setTimeout(function() {
                            $("#time").text("Time: " + index);
                        }, left);
                    }

                    resolve(response.pollingIntervalMillis);
                },
                400: function(resultData) {
                    video.liveChatId = null;
                    video.videoId = null;
                    appendLog("Erro", resultData);
                    main();
                    resolve(false);
                },
                403: function(resultData) {
                    video.liveChatId = null;
                    video.videoId = null;
                    appendLog("Erro", resultData);
                    main();
                    resolve(false);
                },
                404: function(resultData) {
                    video.liveChatId = null;
                    video.videoId = null;
                    appendLog("Erro", resultData);
                    main();
                    resolve(false);
                }
            }
        });
    });
}

function setPoll() {
    var jsonPost = {
        id_bot: channelId,
        dados: poll
    };
    $.ajax({
        type: "POST",
        url: "setNumPoll.php",
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(jsonPost),
        cache: false,
        statusCode: {
            200: function(response) {},
            404: function(resultData) {}
        }
    });
}

// Call the Data API to retrieve the playlist ID that uniquely identifies the
// list of videos uploaded to the currently authenticated user's channel.
function handleAPILoaded() {
    // See https://developers.google.com/youtube/v3/docs/channels/list
    // var request = gapi.client.youtube.channels.list({
    //     mine: true,
    //     part: 'contentDetails'
    // });
    // request.execute(function(response) {
    //     playlistId = response.result.items[0].contentDetails.relatedPlaylists.uploads;
    //     appendLog(playlistId);
    // });
    main();
}

function sendMessage(messageText) {
    if (channelId == "UCCFcBOBr7B3ob4JjyTRdTRQ" && true) { 
        var request = gapi.client.youtube.liveChatMessages.insert({
            part: "snippet",
            snippet: {
                liveChatId: video.liveChatId,
                type: "textMessageEvent",
                textMessageDetails: {
                    messageText: messageText
                }
            }
        });
        request.execute(function(response) {
            //appendLog(response);
        });
    }
}

function ban(channelId, banDurationSeconds) {
    // gapi.client.youtube.liveChatBans.insert({
    //         "part": "snippet",
    //         "resource": {
    //             "snippet": {
    //                 "banDurationSeconds": banDurationSeconds,
    //                 "liveChatId": video.liveChatId,
    //                 "type": "temporary",
    //                 "bannedUserDetails": {
    //                     "channelId": channelId
    //                 }
    //             }
    //         }
    //     })
    //     .then(function(response) {
    //             // Handle the results here (response.result has the parsed body).
    //             //appendLog("Response", response);
    //         },
    //         function(err) {
    //             isBotModerator = false;
    //             console.error("Execute error", err);
    //         });
}

function getParameter(parameterName) {
    var result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function(item) {
            tmp = item.split("=");
            if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        });
    return result;
}
var dueToQuestion = 0;
var timeToQuestion = 0;
var isQuestionRunning = false;

function setQuestion() {
    if (infestacao.isOn) {
        // console.log(isSettingPunishment, infestacao.timeoutQuestion);
        if (!isSettingPunishment && infestacao.timeoutQuestion == null && !infestacao.isActive) {
            var random  = getRandomInt(infestacao.minSec,infestacao.maxSec)
            timeToQuestion = random + plusTimeout;
            var now = Date.now();
            dueToQuestion = timeToQuestion * 1000 + now;
            progressWaitQuestion();
            infestacao.dueTime = now + (timeToQuestion + 135) * 1000;
            infestacao.timeoutQuestion = setTimeout(function() {
                showQuestion();
            }, timeToQuestion * 1000);
            infestacao.timeoutVar = setTimeout(function() {
                clearInfestacao(true);
            }, (timeToQuestion + 135) * 1000);
            appendLog("Next question in: " + timeToQuestion + "s");
            setInfestacao();
        } else {
            setTimeout(function() {
                setQuestion();
            }, 1000);
        }
    }
}
function progressWaitQuestion() {
    var now = Date.now();
    var dif = (dueToQuestion - now) / 1000;
    //console.log(dif,dueToQuestion);
    var percent = 100;
    if (dif > 0) {
        $("#displayProgressQuestion").show();
        percent = 100 - Math.round((dif / timeToQuestion) * 100);
        // console.log(percent, timeToQuestion, dif, Math.round((dif/timeToQuestion)*100));
        $("#progressQuestion").attr("aria-valuenow", "" + percent + "");
        $("#progressQuestion").attr("style", "width: " + percent + "%");
        setTimeout(function() {
            progressWaitQuestion();
        }, 500);
    } else {
        $("#displayProgressQuestion").hide();
    }
}
var plusTimeout = 0;

function clearInfestacao(isMsgChannel) {
    //appendLog(infestacao.timeoutVar, infestacao.timeoutQuestion);
    dueToQuestion = 0;
    infestacao.dueTime = 0;
    clearTimeout(infestacao.timeoutVar);
    clearTimeout(infestacao.timeoutQuestion);
    infestacao.timeoutQuestion = null;
    infestacao.timeoutVar = null;
    if (isMsgChannel) {
        appendLog("INFESTAÇÃO: Tempo esgotado. Sem resposta.");
        sendMessage("INFESTAÇÃO: Tempo esgotado. Sem resposta.");
    } else if (infestacao.isActive) {
        appendLog("INFESTAÇÃO: Pergunta cancelada...");
        sendMessage("INFESTAÇÃO: Pergunta cancelada...");
    }
    //plusTimeout = plus;
    //appendLog("Clear question: <strike>" + infestacao.question + "</strike>");
    $("#displayProgressQuestion").hide();
    $("#displayProgress").hide();
    $("#displayProgressPunishment").hide();
    infestacao.isActive = false;
    isQuestionRunning = false;
    setInfestacao();
    setQuestion();
}

function showQuestion() {
    isQuestionRunning = true;
    var maxQuestion = constInfestacao.questions.length;
    var randomQuestion = getRandomInt(0,(maxQuestion - 1));
    infestacao.question = constInfestacao.questions[randomQuestion].pergunta;
    infestacao.hit.isHit = false;
    infestacao.hit.isSuperchat = false;
    var maxResponse = constInfestacao.questions[randomQuestion].respostas.length;
    var randomResponse = getRandomInt(0,(maxResponse - 1));
    infestacao.word = constInfestacao.questions[randomQuestion].respostas[randomResponse].trim();
    appendLog("QUESTION: " + constInfestacao.questions[randomQuestion].pergunta);
    sendMessage(
        "INFESTAÇÃO: " + constInfestacao.questions[randomQuestion].pergunta
    );
    appendLog("Answer: " + infestacao.word);
    infestacao.isActive = true;
    setInfestacao();
}
 
function randomPunishment(valueDonation) {
    if (valueDonation != 0) {
        var tempPunishments = JSON.parse(JSON.stringify(constInfestacao.punishments));
        tempPunishments.forEach(pun => {
            pun.valueDonation = valueDonation;
        });
        //console.log(tempPunishments);
        var arrPunishment = _.find(tempPunishments, function(o) { return (o.value == o.valueDonation && o.acceptSniper == true); });
        if (typeof arrPunishment === "undefined") {
            arrPunishment = []
            tempPunishments.forEach(pun => {
                if (pun.value <= pun.valueDonation) {
                    arrPunishment.push(pun);
                }
            });
        } else {
            arrPunishment = new Array(arrPunishment);
        }
        //console.log(arrPunishment)
        var randomPunishment = getRandomInt(0, (arrPunishment.length - 1));
        //console.log(randomPunishment);
        infestacao.punishment = null;
        infestacao.punishment = JSON.parse(JSON.stringify(arrPunishment[randomPunishment]));
    } else {
        var randomPunishment = getRandomInt(0, (constInfestacao.punishments.length - 1));
        infestacao.punishment = null;
        infestacao.punishment = JSON.parse(JSON.stringify(constInfestacao.punishments[randomPunishment]));
    }
}

function setWords() {
	var jsonPost = {
		dados: jsonWords
	};
	$.ajax({
		type: "POST",
		url: "setWords.php",
		contentType: "application/json",
		dataType: "json",
		data: JSON.stringify(jsonPost),
		cache: false,
		statusCode: {
			200: function(response) {},
			404: function(resultData) {}
		}
	});
}
function getWords() {
	var jsonPost = {
		dados: infestacao
	};
	$.ajax({
		type: "POST",
		url: "getWords.php",
		contentType: "application/json",
		dataType: "json",
		data: JSON.stringify(jsonPost),
		cache: false,
		statusCode: {
			200: function(response) {
				if (response != null) {
					jsonWords = response;
				} else {
					jsonWords = [];
				}
			},
			404: function(resultData) {
			}
		}
	});
}

function setInfestacao() {
    if (infestacao.isOn) {
        $("#btnInfestacao").attr("class", "btn-sm btn-danger");
        $("#btnInfestacao").text("Desligar Infestação");
         $("#btnReset").show();
    } else {
        $("#btnInfestacao").attr("class", "btn-sm btn-success");
        $("#btnInfestacao").text("Ligar Infestação");
        $("#btnReset").hide();
    }
    var jsonPost = {
        dados: infestacao
    };
    $.ajax({
        type: "POST",
        url: "setInfestacao.php",
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(jsonPost),
        cache: false,
        statusCode: {
            200: function(response) {},
            404: function(resultData) {}
        }
    });
}

function getInfestacao() {
	var jsonPost = {
		dados: infestacao
	};
	$.ajax({
		type: "POST",
		url: "getWords.php",
		contentType: "application/json",
		dataType: "json",
		data: JSON.stringify(jsonPost),
		cache: false,
		statusCode: {
			200: function(response) {
				if (response != null) {
					jsonWords = response;
				} else {
					jsonWords = [];
				}
			},
			404: function(resultData) {
			}
		}
	});
}

function getInfestacao() {
    var jsonPost = {
        dados: infestacao
    };
    $.ajax({
        type: "POST",
        url: "getInfestacao.php",
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(jsonPost),
        cache: false,
        statusCode: {
            200: function(response) {
                if (response != null) {
                    infestacao = response;
                    infestacao.isOn = false;
                    infestacao.hit.isHit = false;
                    infestacao.hit.isSuperchat = false;
                    infestacao.isActive = false;
                } else {
                    resetInfestacao();
                }
                setInfestacao();
            },
            404: function(resultData) {
                resetInfestacao();
            }
        }
    });
}

function setRanking() {
    var jsonPost = {
        dados: ranking
    };
    $.ajax({
        type: "POST",
        url: "setRanking.php",
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(jsonPost),
        cache: false,
        statusCode: {
            200: function(response) {},
            404: function(resultData) {}
        }
    });
}

function getRanking() {
    var jsonPost = {
        dados: infestacao
    };
    $.ajax({
        type: "POST",
        url: "getRanking.php",
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(jsonPost),
        cache: false,
        statusCode: {
            200: function(response) {
                ranking = response;
            },
            404: function(resultData) {}
        }
    });
}
var isSettingPunishment = false;
var queuePunishmentList = [];

function queuePunishment(punishment) {
    if (punishment.isHit) {
        infestacao.isActive = false;
        var userRanking = _.remove(ranking, function(r) {
            return r.author.channelId == punishment.author.channelId;
        });
        if (userRanking.length > 0) {
            var hits = userRanking[0].hits;
            userRanking = {};
            userRanking.author = punishment.author;
            userRanking.hits = ++hits;
            ranking.push(userRanking);
        } else {
            userRanking = {};
            userRanking.author = punishment.author;
            userRanking.hits = 1;
            ranking.push(userRanking);
        }
        queuePunishmentList.unshift(punishment);
        isQuestionRunning = false;
        //console.log(queuePunishmentList);
    } else {
        queuePunishmentList.push(punishment);
    }
    setRanking();
    punishment.punishment = infestacao.punishment;
    // isSettingPunishment = true;
    
}

function verifyPunishment() {
    //console.log("queuePunishment", queuePunishmentList);
    if (queuePunishmentList.length > 0 && !isSettingPunishment && !isQuestionRunning) {
        infestacao.isActive = false;
        var punishment = queuePunishmentList.shift();
        //console.log(punishment);
        appendLog("Applying punishment: " + punishment.author.displayName + " - " + JSON.stringify(punishment.punishment)); 
        infestacao.hit = JSON.parse(JSON.stringify(punishment));
        setInfestacao();
        applyPunishment(punishment);
    } else {
        //appendLog("Waiting scene. Is Setting? ", isSettingPunishment);
    }
    setTimeout(() => {
        verifyPunishment();
    }, 200);
}

var duePunishment = 0;
var punishmentTime = 0;

function applyPunishment(punishment) {
    clearTimeout(infestacao.timeoutVar);
    clearTimeout(infestacao.timeoutQuestion);
    infestacao.timeoutQuestion = null;
    isSettingPunishment = true;
    var now = Date.now();
    duePunishment = punishment.punishment.time * 1000 + now - 500;
    punishmentTime = punishment.punishment.time;
    progressPunishment();
    clearInfestacao(false);
    if (punishment.punishment.type == 'scene') {
        scene = punishment.punishment;
        obs.send("GetCurrentScene", {}, (err, data) => {
            if (err) {
                appendLog("OBS error >> " + err.description);
                duePunishment = 0;
                isSettingPunishment = false;
                clearInfestacao(false);
            } else {
                var oldScene = data.name;
                obs.send(
                    "SetCurrentScene", {
                        "scene-name": scene.name
                    },
                    (err, data) => {
                        if (err) {
                            appendLog("OBS error >> " + err.description);
                            duePunishment = 0;
                            isSettingPunishment = false;
                            clearInfestacao(false);
                        } else {
                            setTimeout(function() {
                                obs.send(
                                    "SetCurrentScene", {
                                        "scene-name": oldScene
                                    },
                                    (err, data) => {
                                        if (err) {
                                            appendLog("OBS error >> " + err.description);
                                            isSettingPunishment = false;
                                            duePunishment = 0;
                                            clearInfestacao(false);
                                        }
                                    }
                                );
                                if (isSettingPunishment) {
                                    appendLog("Punishment ended");
                                }
                                duePunishment = 0;
                                isSettingPunishment = false;
                                infestacao.hit.isSuperchat = false;
                                infestacao.hit.isHit = false;
                                setInfestacao();
                            }, scene.time * 1000);
                        }
                    }
                );
            }
        });
    } else if (punishment.punishment.type == 'text') {
        setTimeout(() => {
            if (isSettingPunishment) {
                appendLog("Punishment ended");
            }
            duePunishment = 0;
            isSettingPunishment = false;
            infestacao.hit.isSuperchat = false;
            infestacao.hit.isHit = false;
            setInfestacao();
        }, punishment.punishment.time * 1000);
    }
}

function progressPunishment() {
    if (isSettingPunishment) {
        var now = Date.now();
        var dif = (duePunishment - now) / 1000;
        //console.log(dif,dueToQuestion);
        var percent = 100;
        if (dif > 0) {
            $("#displayProgressPunishment").show();
            percent = 100 - Math.round((dif / punishmentTime) * 100);
            // console.log(percent, timeToQuestion, dif, Math.round((dif/timeToQuestion)*100));
            $("#progressPunishment").attr("aria-valuenow", "" + percent + "");
            $("#progressPunishment").attr("style", "width: " + percent + "%");
            setTimeout(function() {
                progressPunishment();
            }, 500);
        } else {
            $("#displayProgressPunishment").hide();
        }
    } else { 
        $("#displayProgressPunishment").hide();
    }
}

function resetRanking() {
    ranking = [];
    setRanking();
}
function convertToSlug(string) {
  return string.toString().toLowerCase()
    .replace(/[àÀáÁâÂãäÄÅåª]+/g, 'a')       // Special Characters #1
	.replace(/[èÈéÉêÊëË]+/g, 'e')       	// Special Characters #2
	.replace(/[ìÌíÍîÎïÏ]+/g, 'i')       	// Special Characters #3
	.replace(/[òÒóÓôÔõÕöÖº]+/g, 'o')       	// Special Characters #4
	.replace(/[ùÙúÚûÛüÜ]+/g, 'u')       	// Special Characters #5
	.replace(/[ýÝÿŸ]+/g, 'y')       		// Special Characters #6
	.replace(/[ñÑ]+/g, 'n')       			// Special Characters #7
	.replace(/[çÇ]+/g, 'c')       			// Special Characters #8
	.replace(/[ß]+/g, 'ss')       			// Special Characters #9
	.replace(/[Ææ]+/g, 'ae')       			// Special Characters #10
	.replace(/[Øøœ]+/g, 'oe')       		// Special Characters #11
	.replace(/[%]+/g, 'pct')       			// Special Characters #12
	.replace(/\s+/g, '-')           		// Replace spaces with -
    .replace(/[^\w\-]+/g, '')       		// Remove all non-word chars
    .replace(/\-\-+/g, '-')         		// Replace multiple - with single -
    .replace(/^-+/, '')             		// Trim - from start of text
    .replace(/-+$/, '')            		// Trim - from end of text
    .replace('.', '')
    .replace(':', '')
    .replace(':', '')
    .replace('!', '')
    .replace('?', '')
    .replace('-', '');
}
function getRandomInt(min, max) {
    //console.log(min,max);
    if (Math.ceil(min) == Math.floor(max)) {
        return min;
    } else {
        min = Math.ceil(min);
        max = Math.floor(max);
        return (Math.floor(Math.random() * (max - min + 1)) + min);
    }
}
getInfestacao();
getRanking();
verifyPunishment();