var channelId = "UCCFcBOBr7B3ob4JjyTRdTRQ";
var colorText = "white";
if (getParameter("c")) {
	colorText = "black";
}

var wordsPerPage = 5;

$(document).ready(function() {
	check();
	setInterval(function() {
		check();
	}, 1000);
});

function check() {
	var jsonPost = {};
	$.ajax({
		type: "POST",
		url: "../../getWords.php",
		contentType: "application/json",
		dataType: "json",
		data: JSON.stringify(jsonPost),
		cache: false,
		statusCode: {
			200: function(response) {
				$("#displayWords").empty();
				var total = wordsPerPage;
				var sumWeight = 0;
				var max = 0;
				if (response.length < total) {
					total = response.length;
				}
				for (let index = 0; index < total; index++) {
					const word = response[index];
					sumWeight += word.weight;
					if (word.weight > max) {
						max = word.weight;
					}
				}
				for (let index = 0; index < total; index++) {
					const word = response[index];
					var value = Math.round((word.weight / max) * 100);
					var progressBarStyle = "";
					switch (index) {
						case 0:
							progressBarStyle = "bg-success";
							break;
						case 1:
							progressBarStyle = "";
							break;
						case 2:
							progressBarStyle = "";
							break;
						default:
							progressBarStyle = "bg-info";
							break;
					}
					if (value == 100) {
						progressBarStyle = "bg-success";
					}

					var appendDiv =
						'<div class="row align-middle">' +
						'<div class="col-2"></div>' +
						'<div class="col-2 align-middle text-truncate">' +
						'<div class="text-center" style="color:' +
						colorText +
						'; font-weight:bold; font-size:3vw; vertical-align: middle; margin: 0%; padding: 0%; -webkit-text-stroke: 0px black;"> ' +
						word.text +
						"</div></div>" +
						'<div class="col-6">' +
						'<div class="progress" style="height: 3vw">' +
						'<div class="progress-bar ' +
						progressBarStyle +
						'" role="progressbar" style="width: ' +
						value +
						'%" aria-valuenow="' +
						word.weight +
						'" aria-valuemin="0" aria-valuemax="' +
						max +
						'">' +
						word.weight +
						'<div class="col-2"></div>';
					"</div>" + "</div>" + "</div>";
					$("#displayWords").append(appendDiv);
				}
			}
		},
		404: function(resultData) {}
	});
}

function getParameter(parameterName) {
	var result = null,
		tmp = [];
	location.search
		.substr(1)
		.split("&")
		.forEach(function(item) {
			tmp = item.split("=");
			if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
		});
	return result;
}
