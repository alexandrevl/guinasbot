var channelId = "UCCFcBOBr7B3ob4JjyTRdTRQ";
if (getParameter("c")) {
	channelId = getParameter("c");
}

$(document).ready(function() {
	check();
	setInterval(function() {
		check();
	}, 500);
});

function check() {
	var jsonPost = {
		id_bot: channelId
	};
	$.ajax({
		type: "POST",
		url: "../getInfestacao.php",
		contentType: "application/json",
		dataType: "json",
		data: JSON.stringify(jsonPost),
		cache: false,
		statusCode: {
			200: function(response) {
				//console.log(response);
				if (response.isActive) {
					$("#displayProgress").show();
					$("#question").text(response.question);
					var now = Date.now();
					var dif = response.dueTime - now;
					var percent = 100;
					if (dif >= 0) {
						percent = 100 - Math.round((dif / 135000) * 100);
						//console.log(dif);
					}
					$("#opt0").attr("aria-valuenow", "" + percent + "");
					$("#opt0").attr("style", "width: " + percent + "%");
					//$("#opt0").text("" + percent + "%");
				} else {
					$("#displayProgress").hide();
				}
				if (response.hit.isHit || response.hit.isSuperchat) {
					if (response.hit.isHit) {
						$("#displayHit").css("color", "white");
					}
					if (response.hit.isSuperchat) {
						$("#displayHit").css("color", "yellow");
					}
					$("#imgHit").attr("src", response.hit.author.profileImageUrl);
					if (response.hit.author.displayName.length > 20) {
						$("#textHit").text(response.hit.author.displayName.substring(0, 17) + "...");
					} else {
						$("#textHit").text(response.hit.author.displayName);
					}
					if (response.punishment.type == "text" && response.punishment.word) {
						$("#punishmentHit").text(response.punishment.name + " - " + response.punishment.word);
					} else if (response.punishment.type == "text") {
						$("#punishmentHit").text(response.punishment.name);
					} else if (response.punishment.word) {
						$("#punishmentHit").text(response.punishment.word);
					} else {
						$("#punishmentHit").text("");
					}
					$("#displayHit").show();
				} else {
					$("#displayHit").hide();
				}
			}
		},
		404: function(resultData) {}
	});
}

function getParameter(parameterName) {
	var result = null,
		tmp = [];
	location.search
		.substr(1)
		.split("&")
		.forEach(function(item) {
			tmp = item.split("=");
			if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
		});
	return result;
}
