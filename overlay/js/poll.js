var channelId = 'UCCFcBOBr7B3ob4JjyTRdTRQ';
if (getParameter("c")) {
    channelId = getParameter("c");
}

$(document).ready(function() {
    check();
    setInterval(function() {
        check();
    }, 1000);
});

function check() {
    var jsonPost = {
        "id_bot": channelId
    };
    $.ajax({
        type: 'POST',
        url: "https://bot.mrguinas.com.br/getNumPoll.php",
        contentType: "application/json",
        dataType: "json",
        data: JSON.stringify(jsonPost),
        cache: false,
        statusCode: {
            200: function(response) {
                if (!response[0]) {
                    response[0] = 0;
                }
                if (!response[1]) {
                    response[1] = 0;
                }
                var total = response[0] + response[1];
                var percent = [];
                if (total > 0) {
                    percent[0] = Math.round((response[0] / total) * 100);
                    percent[1] = Math.round((response[1] / total) * 100);
                    $("#opt0").attr('aria-valuenow', "" + percent[0] + "");
                    $("#opt0").attr('style', "width: " + percent[0] + "%");
                    $("#opt0").text("(1) " + percent[0] + "%");
                    $("#opt1").attr('aria-valuenow', "" + percent[1] + "");
                    $("#opt1").attr('style', "width: " + percent[1] + "%");
                    $("#opt1").text("(2) " + percent[1] + "%");
                } else {
                    $("#opt0").attr('aria-valuenow', "" + 50 + "");
                    $("#opt0").attr('style', "width: " + 50 + "%");
                    $("#opt0").text("(1) " + 50 + "%");
                    $("#opt1").attr('aria-valuenow', "" + 50 + "");
                    $("#opt1").attr('style', "width: " + 50 + "%");
                    $("#opt1").text("(2) " + 50 + "%");
                }
            }
        },
        404: function(resultData) {}
    });
}

function getParameter(parameterName) {
    var result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function(item) {
            tmp = item.split("=");
            if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        });
    return result;
}