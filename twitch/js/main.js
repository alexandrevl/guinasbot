let options = {
	options: {
        clientId: "8ju2p4yaaaijh00vntwk79k8rxqd9l"
		debug: false
	},
	connection: {
		secure: true,
		reconnect: true
	},
	// identity: {
	// 	username: "alexandrevl",
	// 	password: "oauth:c3mrhsjemteaelszusvgfkmdulnmzo"
	// },
	channels: ["#esl_brazil"]
};

let client = new tmi.client(options);

// Connect the client to the server..
client.connect();

client.on("message", (channel, userstate, message, self) => {
	// Don't listen to my own messages..
	if (self) return;

	// Handle different message types..
	switch (userstate["message-type"]) {
		case "action":
			// This is an action message..
			break;
		case "chat":
			// This is a chat message..
			//console.log(message);
			$("#chat").append(
				"<div><b>" + userstate["display-name"] + "</b>: " + message + "</div>"
			);
			break;
		case "whisper":
			// This is a whisper..
			break;
		default:
			// Something else ?
			break;
	}
});
